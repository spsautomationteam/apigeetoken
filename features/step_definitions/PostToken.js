/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createToken = function( apickli, cardNo,ExpYear,callback ) {
		var pathSuffix = "/tokens";	
		var url = apickli.domain + pathSuffix;	
	
		var body = {"cardData":{"number":cardNo,"expiration":ExpYear}};
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';		

		var clientSecret = apickli.scenarioVariables.clientSecret;

		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);    
    	
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.vaultResponse.data', 'GUID');	
		
        	callback();
		});
};

var createTokenError = function( apickli,path,callback ) {
		var pathSuffix = path;
		var url = apickli.domain + pathSuffix;	
				
		var body = apickli.requestBody;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';		

		var clientSecret = apickli.scenarioVariables.clientSecret;

		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);        	
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.vaultResponse.data', 'GUID');
			//var GUID = apickli.scenarioVariables.GUID;			
        	callback();
		});
};

var updateToken = function( apickli,GUID,cardNo,ExpYear,callback ) {
		var pathSuffix = "/tokens/"+GUID;
		var url = apickli.domain + pathSuffix;
		var body = {"cardData":{"number":cardNo,"expiration":ExpYear}};
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';		

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "PUT", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.put(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
			
	      	callback();
		});
};

var deleteToken = function( apickli,GUID,callback ) {
		var pathSuffix = "/tokens/"+GUID;
		var url = apickli.domain + pathSuffix;
		var body = '';
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';		

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "DELETE", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		//apickli.delete(pathSuffix, callback);

		apickli.delete(pathSuffix, function (err, response) {
			if ( response ) {
				console.log(response.body);
			}			
	      	callback();
		});
};

module.exports = function () { 

	this.When(/^I Create a token for (.*) credit card number and (.*) expirtation date$/, function (cardNo,ExpYear,callback) {
		createToken(this.apickli,cardNo,ExpYear, callback);
	});
	
	this.When(/^I Update a token for (.*) credit card number and (.*) expirtation date$/, function (cardNo,ExpYear,callback) {
		var GUID = this.apickli.scenarioVariables.GUID;
		//console.log(GUID);
        updateToken(this.apickli,GUID,cardNo,ExpYear, callback);
	
	});
	
	this.When(/^process a request to delete the token$/, function (callback) {
		var GUID = this.apickli.scenarioVariables.GUID;
	    deleteToken(this.apickli,GUID,callback);
		//callback();
	});
	
	this.When(/^I try to create a token for path (.*)$/, function (path,callback) {
		createTokenError(this.apickli,path,callback);
	});
	
	this.When(/^I Update a token for (.*) credit card number and (.*) expirtation date by invalid reference id$/, function (cardNo,ExpYear,callback) {
		var GUID = '123WERF4';
		updateToken(this.apickli,GUID,cardNo,ExpYear, callback);
	});
	
	
	
};

	

