var apickli = require('apickli');
var config = require('../../config/config.json');

var defaultBasePath = config.token.basepath;
var defaultDomain = config.token.domain;

console.log('bankcard api: [' + config.token.domain + ', ' + config.token.basepath + ']');

var getNewApickliInstance = function(basepath, domain) {
	basepath = basepath || defaultBasePath;
	domain = domain || defaultDomain;

	return new apickli.Apickli('https', domain + basepath);
};

exports.getNewApickliInstance = getNewApickliInstance;
