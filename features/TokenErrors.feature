@intg
Feature: TokensError
	As an API consumer
	I want to test all token errors

	@SAGE_Token_API_Errors_100001_Header_Params
    Scenario: Generate Error 100001 - merchantID is not passed
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I Create a token for 4111111111111111 credit card number and 0618 expirtation date
		Then response code should be 400
		And response body path $.code should be 100001
		And response body path $.message should be One or more required headers are missing
	
	@SAGE_Token_API_Errors_100003_Invalid_HMAC
    Scenario: Generate Error 100003 - wrong merchant id is passed
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
		And I set merchantId header to `1234`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I Create a token for 4111111111111111 credit card number and 0618 expirtation date
		Then response code should be 401
		And response body path $.code should be 100003
		And response body path $.message should be Invalid HMAC
			
	@SAGE_Token_API_Errors_100005_Invalid_clientId 
    Scenario: Generate Error 100005 - Invalid client id is passed
        Given I set clientId header to `hOkMcB855YruCK9JQLzpuT9r`
		And I set clientSecret header to `clientSecret`
		And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I Create a token for 4111111111111111 credit card number and 0618 expirtation date
		Then response code should be 401
		And response body path $.code should be 100005
		And response body path $.message should be Missing or invalid Application Identifier
	
	@SAGE_Token_API_Errors_100006_Resource_Not_Found 
    Scenario: Generate Error 100006 - Invalid path is passed
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
		And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "cardData": { "number": "4111111111111111", "expiration": "1018" } }
		When I try to create a token for path /tokenss
		Then response code should be 404
		And response body path $.code should be 100006
		And response body path $.message should be Resource not found
		
	@SAGE_Token_API_Errors_100007_Invalid_Message_Format
    Scenario: Generate Error 100007 - Invalid body is passed
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
		And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "cardData": { "number": "4111111111111111", "expiration": "1018" } };
		When I try to create a token for path /tokens
		Then response code should be 400
		And response body path $.code should be 100007
		And response body path $.message should be Invalid message request format
		
	@SAGE_Token_API_Errors_100008_Invalid_message_request_content 
    Scenario: Generate Error 100008 - Invalid body is passed
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
		And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "cardData": { "expiration": "1018"} }
		When I try to create a token for path /tokens
		Then response code should be 400
		And response body path $.code should be 100008
		And response body path $.message should be Invalid message request content
		
	@SAGE_Token_API_Errors_400000_Invalid_card_number
    Scenario: Generate Error 400000 - Invalid card number is passed
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
		And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "cardData": { "number": "41111111", "expiration": "1018" } }
		When I try to create a token for path /tokens
		Then response code should be 420
		And response body path $.code should be 400000
		And response body path $.detail should be INVALID CARDNUMBER
		
	@SAGE_Token_API_Errors_400000_Invalid_Expiry_Date
    Scenario: Generate Error 400000 - Invalid expiry date is passed
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
		And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "cardData": { "number": "4111111111111111", "expiration": "18" } }
		When I try to create a token for path /tokens
		Then response code should be 420
		And response body path $.code should be 400000
		And response body path $.detail should be INVALID EXPIRATION DATE
		
	@SAGE_Token_API_Errors_400000_UNABLE_TO_LOCATE
    Scenario: Generate Error 400000 - Invalid reference is passed
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
		And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "cardData": { "number": "4111111111111111", "expiration": "1018" } }
		When I try to create a token for path /tokens
		And I Update a token for 4111111111111111 credit card number and 1018 expirtation date by invalid reference id
		Then response code should be 404
		And response body path $.code should be 100006
		And response body path $.detail should be UNABLE TO LOCATE
		
	@SAGE_Token_API_Errors_400000_UNABLE_TO_VERIFY_VAULT_SERVICE
    Scenario: Generate Error 400000 - Invalid merchant key is passed
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
		And I set merchantId header to `merchantId`
        And I set merchantKey header to `454564`
		And I set Content-Type header to "application/json"
		And I set body to { "cardData": { "number": "4111111111111111", "expiration": "1018" } }
		When I try to create a token for path /tokens
		Then response code should be 420
		And response body path $.code should be 400000
		And response body path $.detail should be UNABLE TO VERIFY VAULT SERVICE
		
				
	@SAGE_Token_API_Errors_400000_VAULT_SERVICE_EXCEPTION 
    Scenario: Generate Error 400000 - Invalid merchant key is passed
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
		And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "cardData": { "number": "abcvddfg", "expiration": "1018" } }
		When I try to create a token for path /tokens
		Then response code should be 420
		And response body path $.code should be 400000
		And response body path $.detail should be VAULT SERVICE EXCEPTION 