@intg
Feature: Tokens
	As an API consumer
	I want to create a token for a credit card number and expirtation date

	@post-token_forAllCards
    Scenario Outline: Create a token for different credit card number and expirtation date
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I Create a token for <cardNo> credit card number and <ExpYear> expirtation date
		Then response code should be 200
		And response body should contain SUCCESS
		
	Examples:
		|	CardName		|	cardNo	 			|	ExpYear	|										
		|	Visa		    |	4111111111111111	|	0617	| 
		|	MasterCard      |	5499740000000057	|	0718 	|
		|	Discover	    |	6011000993026909	|	0819	|
		|	Amex		    |	371449635392376 	|	0920	|
		
	@put-token_forAllCards
    Scenario Outline: Process a request to update an existing token for a credit card number and expiration date
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I Create a token for <cardNo> credit card number and <ExpYear> expirtation date
		And I Update a token for <cardNo> credit card number and <UpdateExpYear> expirtation date
		Then response code should be 200
		And response body should contain SUCCESS
		
	Examples:
		|	CardName		|	cardNo	 			|	ExpYear	|	UpdateExpYear  |									
		|	Visa		    |	4111111111111111	|	0617	| 	0618		   |	
		|	MasterCard      |	5499740000000057	|	0718 	|	0719		   |	
		|	Discover	    |	6011000993026909	|	0819	|	0820		   |	
		|	Amex		    |	371449635392376 	|	0920	|	0921		   |	
		
	
	@delete-token_forAllCards
    Scenario Outline: Process a request to delete an existing token
        Given I set clientId header to `clientId`
		And I set clientSecret header to `clientSecret`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I Create a token for <cardNo> credit card number and <ExpYear> expirtation date
		And process a request to delete the token
		#Then response code should be 200
		And response body should contain DELETED
		
	Examples:
		|	CardName		|	cardNo	 			|	ExpYear	|								
		|	Visa		    |	4111111111111111	|	0617	|
		|	MasterCard      |	5499740000000057	|	0718 	|
		|	Discover	    |	6011000993026909	|	0819	|
		|	Amex		    |	371449635392376 	|	0920	|
 
	
		